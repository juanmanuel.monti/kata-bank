import domain.Amount
import domain.BankAccount
import domain.OverdraftException
import junit.framework.Assert.assertEquals
import org.junit.Test

class WithdrawMoneyShould {

    private lateinit var paskuAccount:BankAccount
    private val accountId = 1
    private val clientId = "pasku123"
    private val initialAmount = Amount(100)
    private val withdrawAmount = Amount(60)
    private val overdraftAmount = Amount(110)
    private val expectedBalance = Amount(40).getValue()

    @Test
    fun `update user account balance`() {
        givenAClientWithInitialAccountBalance()
        whenMakesAWithdraw()
        thenTheBalanceIsDecreased()
    }

    @Test(expected = OverdraftException::class)
    fun `avoid overdraft withdrawal`() {
        givenAClientWithInitialAccountBalance()
        whenMakesAOverdraftedWithdraw()
    }

    private fun givenAClientWithInitialAccountBalance() {
        paskuAccount = BankAccount(clientId,accountId,initialAmount)
    }

    private fun whenMakesAWithdraw() {
        paskuAccount.withdraw(withdrawAmount)
    }

    private fun whenMakesAOverdraftedWithdraw() {
        paskuAccount.withdraw(overdraftAmount)
    }

    private fun thenTheBalanceIsDecreased() {
        val actualBalance = paskuAccount.getBalance()
        assertEquals(expectedBalance,actualBalance)
    }
}