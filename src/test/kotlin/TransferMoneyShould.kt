import domain.BankAccount
import domain.AccountTransaction
import domain.Amount
import domain.OverdraftException
import junit.framework.Assert
import org.junit.Test

class TransferMoneyShould {

    private lateinit var paskuAccount: BankAccount
    private lateinit var kuspaAccount: BankAccount
    private lateinit var accountTransaction: AccountTransaction
    private val paskuClientId = "pasku123"
    private val kuspaClientId = "kuspa456"
    private val accountId = 1
    private val initialPaskuBalance = Amount(100)
    private val initialKuspaBalance = Amount(0)
    private val transferAmount = Amount(40)
    private val overdraftedTransferAmount = Amount(110)
    private val expectedPaskuBalance = Amount(60).getValue()
    private val expectedKuspaBalance = Amount(40).getValue()

    @Test
    fun `transfer money`() {
        givenTwoClientsWithInicialAccountBalance()
        whenMakesATransferBetweenTwoClientAccounts()
        thenTheOriginBalanceIsDecreased()
        thenTheDestinationBalanceIsIncreased()
    }

    @Test(expected = OverdraftException::class)
    fun `avoid transfer without founds`() {
        givenTwoClientsWithInicialAccountBalance()
        whenMakesAnOverdraftedTransferBetweenTwoClientAccounts()
    }

    private fun givenTwoClientsWithInicialAccountBalance() {
        paskuAccount = BankAccount(paskuClientId, accountId, initialPaskuBalance)
        kuspaAccount = BankAccount(kuspaClientId, accountId, initialKuspaBalance)
    }

    private fun whenMakesATransferBetweenTwoClientAccounts() {
        accountTransaction = AccountTransaction(paskuAccount, kuspaAccount)
        accountTransaction.transfer(transferAmount)
    }

    private fun whenMakesAnOverdraftedTransferBetweenTwoClientAccounts() {
        accountTransaction = AccountTransaction(paskuAccount, kuspaAccount)
        accountTransaction.transfer(overdraftedTransferAmount)
    }

    private fun thenTheOriginBalanceIsDecreased() {
        val kuspaActualBalance = kuspaAccount.getBalance()
        Assert.assertEquals(expectedKuspaBalance, kuspaActualBalance)
    }

    private fun thenTheDestinationBalanceIsIncreased() {
        val paskuActualBalance = paskuAccount.getBalance()
        Assert.assertEquals(expectedPaskuBalance, paskuActualBalance)
    }
}