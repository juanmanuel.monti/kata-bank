import domain.Amount
import domain.BankAccount
import domain.OnlyPositiveAmountsException
import junit.framework.Assert.assertEquals
import org.junit.Test

class DepositMoneyShould {

    private lateinit var paskuAccount: BankAccount
    private val clientId = "pasku123"
    private val accountId = 1
    private val initialAmount = Amount(100)
    private val depositAmount= Amount(10)
    private val expectedBalance = Amount(110).getValue()

    @Test
    fun `increase user balance`() {
        givenAClientWithInicialAccountBalance()
        whenMakesADeposit()
        thenTheBalanceIsIncreased()
    }

   @Test(expected = OnlyPositiveAmountsException::class)
    fun `avoid negative deposit amounts`() {
       givenAClientWithInicialAccountBalance()
       whenMakesANegativeDeposit()
    }

    private fun givenAClientWithInicialAccountBalance() {
        paskuAccount = BankAccount(clientId, accountId, initialAmount)
    }

    private fun whenMakesADeposit() {
        paskuAccount.deposit(depositAmount)
    }

    private fun whenMakesANegativeDeposit() {
        paskuAccount.deposit(Amount(-10))
    }

    private fun thenTheBalanceIsIncreased() {
        val actualBalance = paskuAccount
        assertEquals(expectedBalance, actualBalance.getBalance())
    }
}