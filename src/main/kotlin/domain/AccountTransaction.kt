package domain

import domain.BankAccount

class AccountTransaction(val accountOrigin: BankAccount, val accountDestination: BankAccount){

    fun transfer(amount: Amount){
        accountOrigin.withdraw(amount)
        accountDestination.deposit(amount)
    }
}