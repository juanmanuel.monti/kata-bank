package domain

class Amount(private val amount: Int) {
    init {
        if (amount < 0) {
            throw OnlyPositiveAmountsException()
        }
    }
    fun getValue():Int{
        return amount
    }
}