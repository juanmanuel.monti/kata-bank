package domain

class BankAccount(clientId: String, val Id: Int, amount: Amount) {

    private var balance = amount.getValue()

    fun deposit(amount: Amount) {
        balance += amount.getValue()
    }

    fun getBalance(): Int {
        return balance
    }

    fun withdraw(amount: Amount) {
        if(balance<amount.getValue()){
            throw OverdraftException()
        }
        balance -= amount.getValue()
    }
}